SOURCES=$(wildcard *.md)
TARGETS=$(foreach source,$(SOURCES),html/$(source:.md=.html))
SOURCE_CSS=style.css
STATIC_DOCS= \
  2020-03-31-Prevention-teletravail.pdf

build: $(TARGETS)

html/:
	mkdir -p html

html/%.html: %.md | html/ # to my future self: the pipe indicates an "order-only dependency"
	pandoc --standalone --css=$(SOURCE_CSS) $< -o $@
# automatically insert HTML content for visible anchors	(see style.css)
	sed -i $@ -e 's,\(<h[0-9] id="\(.*\)">\),\1<a class="section-anchor" href="#\2" aria-hidden="true"></a>,g'
	sed -i $@ -e 's,href="\([^/]*\).md",href="\1.html",g'

.PHONY: clean
clean:
	rm $(TARGETS)
	rmdir html

# Hardcoded values for myself (Gabriel Scherer), sorry...
deploy: $(TARGETS) $(SOURCE_CSS) $(STATIC_DOCS)
	scp $^ login-lix:.webdir/tmp/teletravail-isole
