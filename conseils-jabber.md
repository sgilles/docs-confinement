% Conseils d'utilisation pour le serveur Jabber INRIA

Voir aussi la [page d'information du wiki INRIA (sur l'installation de
client et sur la connection au réseau
INRIA)](https://wiki.inria.fr/support/R%C3%A9seau_de_messagerie_instantan%C3%A9e_Jabber)

(Le contenu ci-dessous est rédigé et contribué par Gaëtan Leurent.)

(English version below after the `===`.)

Attention, il faut bien utiliser le domaine inria.fr pour votre compte,
mais le serveur est jabber.inria.fr.  Avec certains clients, il faut
chercher dans les options avancées pour pouvoir configurer les deux
séparement.  Certains clients ont aussi un seul champ adresse XMPP à
remplir avec votre adresse mail prenom.nom@inria.fr, au lieu d'avoir un
champ username et un champ domain.

Pour ceux qui utilisent déjà Thunderbird pour lire leurs mail, vous
pouvez aussi l'utiliser comme client Jabber:
- ouvrir la page "account setting"
- Ouvrir le menu Account Actions / Add chat account
- Choisir XMPP
- Username: prenom.nom
- Domain: inria.fr
- Ensuite il faut aller changer les paramètres du compte, et mettre
"jabber.inria.fr" dans la case "Server"
Il y a un bouton "Chat" en dessous de la barre d'onglets pour accéder à
la fenêtre de chat.

Le mot de passe à utiliser est votre mot de passe Inria.  Une fois que
vous êtes connecté, vous pouvez échanger des messages avec n'importe
quel compte Inria (s'il est connecté...).

Pour rejoindre un groupe "toto", il faut chercher une option comme "join
chat" ou "join public channel", et indiquer "toto" comme nom de salle,
et "conference.inria.fr" comme serveur (ou toto@conference.inria.fr
comme adresse, suivant les clients).

======================================================================

The parameters to connect to your inria account are:
Username: first.lastname
Domain: inria.fr
Server: jabber.inria.fr

Be careful, you need to use two different adresses: inria.fr as the
domain, and jabber.inria.fr for the server.  With some clients, you need
to go to advanced options to specify them separately.  Some client also
merge the username and domain as a single XMPP address, which will be
the same as your email address: first.lastname@inria.fr.

If you use Thunderbird for your emails, you can also use it to access
jabber:
- Open "account setting"
- Go to Account Actions / Add chat account
- Choose XMPP
- Username: first.lastname
- Domain: inria.fr
- Then, go to the account settings, and change the server to
"jabber.inria.fr"
There is a "Chat" button below the tabs bar that opens the chat window.

You have to connect with your Inria password, and you can chat with any
Inria user.

To join the chat "foo", look for an option such as "Join chat" or "Join
public channel", and enter "foo" as room name and conference.inria.fr
as server (or foo@conference.inria.fr as chat address in some clients).
