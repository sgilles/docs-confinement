% Conseils d'utilisation Zulip

[Zulip](https://zulipchat.com/) est un logiciel libre de discussions en ligne (asynchrone, style "chat"). L'interface est en anglais, mais simple à utiliser.

Ses mainteneurs proposent un service d'hébergement centralisé gratuit: <https://zulipchat.com/plans/>. Les données sont stockées sur leurs serveurs, et il est donc à éviter pour des discussions confidentielles. Voir la [politique de confidentialité](https://zulipchat.com/privacy/), et la [liste des tierces parties](https://zulipchat.com/help/gdpr-compliance#third-parties) avec qui Zulip doit partager des données pour fournir ce service. Voir nos [conseils d'utilisation Zulip](conseils-zulip.md).

L'hébergement gratuit pose une limite sur la taille de l'historique de conversation qui est conservé : pour Zulip par exemple, pas plus de 10K messages conservés. Ce n'est pas un soucis pour un lieu de convivialité virtuel (on ré-écoute rarement les conversations du café d'il y a un mois).)

Quand on crée un Zulip de discussion pour son équipe/service, les contenus sont inaccessibles aux autres. Pour que ses collègues y aient accès il faut les "inviter", en allant dans les paramètres (icône de roue dentée en haut à droite), "Manage Organization", onglet "Invitations", bouton "Invite more users"; on peut lister des adresses emails ou, plus simplement, générer un lien à utiliser pour se connecter (General Invite Link), à partager avec son équipe ou service.
