% Télétravail isolé à l'INRIA: pratiques et outils ? Retours d'expérience

Bonjour à tous et toutes,

Le télétravail confiné a changé, souvent radicalement, nos façons de travailler. Il comporte des risques d'isolement professionnel (perte de lien avec les collègues) pour notre santé, surtout dans un contexte de vie qui est en ce moment stressant et difficile pour beaucoup d'entre nous.

En tant qu'élu-e-s CLHSCT INRIA, nous réfléchissons à comment limiter ces risques. Ce document contient des retours d'expériences sur des pratiques et outils de tet préconisations sur les méthodes et outils de travail.

Nous encourageons tous les agents à s'emparer de ces idées et à nous faire des retours d'expérience (par email à gabriel.scherer@inria.fr, ou sur gitlab).

Si vous pensez qu'une pratique ci-dessous pourrait avoir du sens dans votre équipe, vous pouvez la suggérer. Chaque équipe ou service a un style propre, il peut être utile d'expérimenter avec une idée, sur base de volontariat, en état prêt-e à la laisser tomber si elle ne marche pas pour vous en pratique.

## Résumé

Nous recommandons, à l'échelle d'une équipe ou d'un service:

- Deux réunions synchrones par semaine (audio ou vidéo)
- Un "lieu de convivialité virtuel" sous forme de chat en ligne, pour garder un lien social plus informel.
- À chacun de prendre soin de sa santé en cette période difficile; à minima, faire des pauses, ménager des moments de détente, éviter la surcharge.

Vous trouverez ci-dessous des conseils plus détaillés et des propositions d'outils pour répondre aux différents besoins.

# Principes

## Réduire l'isolement professionnel

Notre environnement professionnel habituel permet des discussions de travail , et aussi des discussions plus plus informelles, au cours de la journée, souvent dans des lieux de
convivialité (salle café, cantine/cafétaria). Les deux aspects sont importants pour travailler dans de bonnes conditions.

On parle de discussions "synchrones" quand tout le monde doit être présent en même temps dans un créneau de temps fixé (téléphone, visio: on doit se synchroniser), et de discussions "asynchrones" quand les messages peuvent s'étaler dans le temps, chacun participant selon sa disponibilité (échanges par email, SMS).

Pour maintenir un lien social avec nos collègues, ces deux besoins demandent des outils différents:

- Une solution pour des *discussions synchrones*, fixées dans le temps, audio ou vidéo (si disponible).
  On peut avoir des discussions de travail ou plus informelles (prendre des nouvelles les uns des autres pendant une réunion d'équipe).

- Des *lieux de convivialités virtuels* pour garder un lien informel. Pour cela, nous avons expérimenté avec diverses solution asynchrones de "chat en ligne" (discussions textes) à l'échelle de l'équipe ou du service, pouvant ponctuellement se transformer en discussion audio/vidéo spontanées. (Pour les discussions asynchrones de travail, l'email reste souvent l'outil privilégié.)

Nous avons deux *suggestions* qui pourraient vous intéresser (à moduler selon vos envies et besoins):

- Avoir une réunion synchrone de groupe deux fois par semaine, pour se retrouver, faire le point et discuter.
  (Laisser une place aux discussions non-travail.)

- Mettre en place un lieu de convivialité virtuel en utilisant des outils qui peuvent convenir aux membres de l'équipe ou service.

Pour beaucoup notre télétravail est dégradé, en compétition avec la garde d'enfants ou le soin de personnes malades ou en difficulté. La participation à ces réunions et ces espaces ne devrait donc pas avoir un caractère obligatoire, mais se faire sur la base du volontariat et de la disponibilité. (Par exemple, essayer d'avoir deux réunions par semaine peut permettre aux gens qui sont de fait à mi-temps d'espérer assister à l'une des deux, sans se sentir coupables de manquer l'unique rendez-vous de l'équipe ou du service.)


## Santé au télétravail

Notre *santé physique et mentale* est mise à l'épreuve en ce moment, et il est important d'en être conscient. Les recommandations de santé au télétravail s'appliquent, dans des conditions plus difficiles que le télétravail habituel :

- Faire au mieux pour organiser un espace de travail de façon confortable, qui ne crée pas de risques de mal de dos, garantir une bonne luminosité dans l'espace de travail (pas seulement l'écran) etc.

- Prendre des pauses régulièrement, _surtout_ quand on est surchargé-e de travail.

- Faire au mieux pour conserver une forme activité physique régulière.

- Se ménager, protéger des périodes de non-travail, de détente.

Voir à ce sujet les fiches intranet [Télétravail -- Santé](https://intranet.inria.fr/Vie-pratique/Infos-pratiques/Teletravail/Sante-Teletravail) et la [Fiche risque: Travail sur Écran](https://intranet.inria.fr/Vie-pratique/Sante-securite-prevention/Travail-sur-ecran). Nous conseillons aussi la lecture la présentation [Prévention des risques du télétravail (PDF)](2020-03-31-Prevention-teletravail.pdf) diffusée par l'École Polytechnique, qui aborde aussi les risques psychosociaux spécifiques au télétravail, en particulier en situation d'isolement.

Au sujet des risques psychologiques, l'INRIA met à disposition de tous les agents et membres d'équipe INRIA (y compris les étudiants et stagiaires) des services téléphoniques gratuits de soutien psychologique, disponibles aussi en anglais. Voir la [page intranet à ce sujet](https://intranet.inria.fr/Actualite/Crise-sanitaire-Covid-19-Coronavirus-Soutien-et-accompagnement-psychologique). En cette période de confiné, ne pas hésiter à mentionner ce service à nos collègues isolés.

# Outils

La DSI de l'INRIA maintient une suite de services et d'outils numériques qui sont évidemment le choix à privilégier quand ils sont adaptés au besoin. Mais l'offre interne ne répond pas forcément à tous les besoins, soit qu'un certain type de service n'existe pas, soit parce qu'il est saturé en ce moment. Nous mentionnerons aussi des outils externes pour ces cas-là, en étant transparents sur les problématiques de vie privée et sécurité de données.

Toutes les plateformes d'outils collaboratifs (internes comme externes) sont très demandées en ce moment, il faut se préparer à changer d'outil au rythme des (in)disponibilités, et faire un effort d'usage raisonné des ressources : utiliser le texte quand c'est possible, puis l'audio quand cela apporte du confort, et limiter au maximum la vidéo (surtout au-delà de deux personnes).

Voir aussi la [fiche intranet](https://intranet.inria.fr/Actualite/Crise-sanitaire-Covid-19-Coronavirus-Regles-d-utilisation-des-outils-collaboratifs-en-situation-de-teletravail-massif) de la DSI sur l'usage responsable des outils collaboratifs.

## Discussions synchrones

### Audio

L'INRIA met à disposition des *lignes téléphoniques* qui permettent des conférences audio à plusieurs: on passe par Zimbra pour réserver une ligne, qui donne un numéro que les participant-e-s appellent pour se connecter (des collaborateurs externes peuvent participer). Selon notre expérience depuis le début du confinement, ce service marche globalement bien. Voir [la page de documentation du wiki](https://wiki.inria.fr/support/Syst%C3%A8me_audioconf%C3%A9rence), qui explique la procédure à suivre.

Pour les usages qui demandent d'appeler un numéro de téléphone (dont le service audio INRIA), il est possible de téléphoner depuis son ordinateur (avec son micro/casque habituel, le confort pour la prise de notes, etc.) en se procurant une "ligne SIP"; OVH [loue des lignes SIP](https://www.ovhtelecom.fr/telephonie/voip/decouverte.xml) pour 1.30€ par mois. Pour appeler un numéro, on peut utiliser le logiciel libre Twinkle. (Pour composer des touches pendant un appel, par exemple pour se connecter à une réunion INRIA, c'est le bouton "Dtmf".)

Pour des discussions non-INRIA, avec vos proches par exemple, nous avons utilisé *Mumble*, un logiciel libre de discussion audio conçu au départ pour le jeu vidéo. Il marche très bien et demande peu de bande passante, il est en particulier très facile à auto-héberger sur sa machine personnelle. Framasoft héberge en ce moment un serveur Mumble à destination des particuliers et associations (pas pour les usages INRIA), voir [leur page et leur documentation](https://framablog.org/2020/03/19/mumble-framatalk-un-serveur-pour-parler-a-plusieurs/).


### Vidéo

Les services vidéo demandent des tranferts de données beaucoup plus lourds que les services audio, ils sont plutôt à éviter s'il est facile de faire une réunion en audio seul. Parfois la vidéo apporte vraiment quelque chose (et il est agréable de voir ses collègues de temps en temps).

L'INRIA met à disposition un service *visio.inria.fr*; il a en ce moment des difficultés à tenir la charge, voir la [fiche intranet](https://intranet.inria.fr/Actualite/Crise-sanitaire-Covid-19-Coronavirus-Regles-d-utilisation-des-outils-collaboratifs-en-situation-de-teletravail-massif) pour les règles de bonne utilisation du service.

RENATER (un réseau maintenu par les établissements de l'enseignement supérieur) propose aussi deux offres de visio-conférence, [Rendez-Vous](https://rendez-vous.renater.fr/home/) et [RENAvisio](https://services.renater.fr/renavisio/index), mais elles souffrent aussi de problèmes de saturation. Vos instituts ou services partenaires ont aussi des services (au CNRS [My Com](https://www.ods.cnrs.fr/my_com.php), basé sur Skype Entreprise.)

Si ces services ne sont pas disponibles, nos collègues hors-INRIA utilisent souvent une panoplie d'outils commerciaux (mais pas forcément payants) du marché, comme Zoom, Bluejeans, Microsoft Teams, ou Google Hangout. Mais la sécurité des données (pour les discussions professionnelles et surtout confidentielles) et le respect de la vie privée (pour tout le monde) n'est pas garanti, et souvent mis en danger par ces outils; voir par exemple [cette page](https://protonmail.com/blog/zoom-privacy-issues/) qui alerte sur les fonctionnalités de "attention tracking" de Zoom qui sont très invasives.

Nous recommandons plutôt les offres de visio-conférence basées sur du logiciel libre, en particulier *Jitsi*. Scaleway (une entreprise d'hébergement française, du groupe Free/Iliad) propose bénévolement une instance pendant la crise Coronavirus, <https://ensemble.scaleway.com/>, qui nous semble l'alternative "la moins pire" si visio.inria.fr et Renater sont indisponibles (à utiliser aussi avec vos proches !). Pour les personnes techniques, est aussi possible d'héberger son propre serveur Jitsi dans le cloud (pas vraiment faisable à domicile à cause du besoin en bande passante), voir par exemple [la documentation Scaleway](https://www.scaleway.com/en/docs/setting-up-jitsi-meet-videoconferencing-on-debian-stretch/).

Note: Jitsi fonctionne mieux avec les navigateurs Chrome ou Chromium que Firefox, car il exploite des fonctionnalités non-standard de partage de flux vidéo. Encouragez vos participants à utiliser Chromium si possible.

## Discussions asynchrones > lieux de convivialité virtuels

Nous recommandons un outil de style "chat en ligne" pour garder un contact avec ses collègues pendant la journée. Ces outils sont "asynchrones", ils ne demandent pas une attention constante; on peut s'y connecter à un moment de son travail, saluer les collègues qui s'y trouvent, et aller regarder de temps à autre. C'est aussi un lieu pour poser une question rapide ou partager une information utile, ou avoir une conversation texte avec un-e collègue (pour une discussion plus longue ou plus délicate, on peut préférer un outil de discussion syncrhone).

Les outils de chat permettent d'avoir plusieurs canaux/lieux. Nous utilisons typiquement:

- un canal "Bonjour" pour se saluer quand on commence à travailler et échanger des nouvelles d'ordre général (comme on ferait quand on croise ses collègues)
- un canal "Détente", explicitement informel, adapté pour les discussions non-travail (en ce moment, il est utile aussi de prendre des nouvelles)
- un canal pour les discussions de travail

Nous avons de l'expérience avec deux logiciels libres de bonne qualité, Mattermost et Zulip, que nous recommandons sans hésiter. Ils s'utilisent dans un navigateur web, sans avoir besoin d'installer un client sur les machines, et sont faciles à utiliser, même pour des personnels non-techniques.

- la DSI est en train de travailler à déployer une instance de *Mattermost* à l'INRIA, prévue pour début avril. Quand elle sera disponible nous recommanderons évidemment son usage. En attendant, nous ne connaissons pas d'offre d'hébergement gratuite et respectueuse. Il est possible d'auto-héberger une instance ( voir la [documentation de Scaleway](https://www.scaleway.com/en/docs/installing-mattermost-messaging-ubuntu-bionic/) ), mais cela demande un peu de travail d'un-e volontaire dans votre équipe ou service.

- *Zulip* propose un hébergement centralisé gratuit: <https://zulipchat.com/> -- l'interface est en anglais, mais simple à utiliser. Il existe aussi des applications Zulip pour téléphones mobiles. Le logiciel est libre, mais le serveur gratuit doit stocker les données que l'on met sur le chat, et il est donc à éviter pour des discussions confidentielles. Voir la [politique de confidentialité](https://zulipchat.com/privacy/), et la [liste des tierces parties](https://zulipchat.com/help/gdpr-compliance#third-parties) avec qui Zulip doit partager des données pour fournir ce service. Voir nos [conseils d'utilisation Zulip](conseils-zulip.md).

Un autre outil disponible est *Jabber*, un système de chat (façon IRC) pour lequel l'INRIA héberge un serveur. Jabber demande d'installer un client en local et est légèrement moins facile d'accès que Mattermost ou Zulip pour des personnes non-techniques, mais c'est un bon système hébergé dans notre institut, que plusieurs équipes utilisent. Voir la [page Jabber du wiki INRIA](https://wiki.inria.fr/support/R%C3%A9seau_de_messagerie_instantan%C3%A9e_Jabber), et les [conseils d'utilisation de Gaëtan Leurent](conseils-jabber.md).


# Aller plus loin

Ce document a été principalement rédigé par Gabriel Scherer, avec les retours des autres représentant-e-s du CLHSCT de l'INRIA Saclay, en intégrant les suggestions reçues par la suite.

## Sources et partage

Un rendu web de ce document est [ici](http://www.lix.polytechnique.fr/Labo/Gabriel.Scherer/tmp/teletravail-isole/teletravail-isole-retour-d-experience.html),
les sources Markdown sont disponibles [ici](https://gitlab.inria.fr/gscherer/docs-confinement/-/blob/master/teletravail-isole-retour-d-experience.md).

N'hésitez pas à suggérer des améliorations, ou à le reproduire et le modifier pour un contexte non-INRIA par exemple.
(Il est trop difficile d'écrire un document unique qui convienne à la fois à l'INRIA et en dehors.)

(Nous plaçons ce document sous une licence Creative Commons "domaine public", CC-0.)
